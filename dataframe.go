package df

import (
	"context"
	"encoding/csv"
	"fmt"
	"os"
)

// DataFrame is a struct that holds the data and metadata of a dataframe
type DataFrame struct {
	Columns []string
	Data    map[string][]interface{}
}

// NewDataFrame creates a new DataFrame
func NewDataFrame() *DataFrame {
	return &DataFrame{
		Columns: []string{},
		Data:    make(map[string][]interface{}),
	}
}

// NewDataFrameFromCSV creates a new DataFrame from a CSV file
func NewDataFrameFromCSV(f *os.File) (*DataFrame, error) {
	d := NewDataFrame()
	err := d.FromCSV(f)
	return d, err
}

// AddColumn
func (d *DataFrame) AddColumn(name string, data []interface{}) {
	d.Columns = append(d.Columns, name)
	d.Data[name] = data
}

// AddRow
func (d *DataFrame) AddRow(row map[string]interface{}) {
	for c, v := range row {
		d.Data[c] = append(d.Data[c], v)
	}
}

// FillNA
func (d *DataFrame) FillNA(value interface{}) {
	for _, c := range d.Columns {
		for i, v := range d.Data[c] {
			if v == nil {
				d.Data[c][i] = value
			}
		}
	}
}

// DropNA
func (d *DataFrame) DropNA() {
	for _, c := range d.Columns {
		var data []interface{}
		for _, v := range d.Data[c] {
			if v != nil {
				data = append(data, v)
			}
		}
		d.Data[c] = data
	}
}

// DropDuplicates
func (d *DataFrame) DropDuplicates() {
	for i := 0; i < len(d.Data[d.Columns[0]]); i++ {
		for j := i + 1; j < len(d.Data[d.Columns[0]]); j++ {
			duplicate := true
			for _, c := range d.Columns {
				if d.Data[c][i] != d.Data[c][j] {
					duplicate = false
					break
				}
			}
			if duplicate {
				for _, c := range d.Columns {
					d.Data[c] = append(d.Data[c][:j], d.Data[c][j+1:]...)
				}
				j--
			}
		}
	}
}

// FromCSV
func (d *DataFrame) FromCSV(f *os.File) error {

	r := csv.NewReader(f)

	header, err := r.Read()
	if err != nil {
		return fmt.Errorf("error reading header: %v", err)
	}

	d.Columns = header

	for {
		row, err := r.Read()
		if err != nil {
			break
		}

		for i, v := range row {
			d.Data[header[i]] = append(d.Data[header[i]], v)
		}
	}

	return nil
}

// ToCSV
func (d *DataFrame) ToCSV(f *os.File) error {

	w := csv.NewWriter(f)

	err := w.Write(d.Columns)
	if err != nil {
		return fmt.Errorf("error writing header: %v", err)
	}

	for i := 0; i < len(d.Data[d.Columns[0]]); i++ {
		var row []string
		for _, c := range d.Columns {
			row = append(row, fmt.Sprintf("%v", d.Data[c][i]))
		}
		if err := w.Write(row); err != nil {
			return fmt.Errorf("error writing row: %v", err)
		}
	}

	w.Flush()

	return nil

}

// FromSqlQuery executes a SQL query and stores the result in the dataframe
func (d *DataFrame) FromSqlQuery(db DBTX, ctx context.Context, query string, args ...any) error {

	rows, err := db.QueryWithContext(ctx, query, args...)
	if err != nil {
		return fmt.Errorf("error executing query: %v", err)
	}

	columns, err := rows.Columns()
	if err != nil {
		return fmt.Errorf("error getting columns: %v", err)
	}
	d.Columns = columns

	// initialize data map
	d.Data = make(map[string][]interface{})

	// iterate over rows
	for rows.Next() {

		var row []interface{}
		for range columns {
			row = append(row, new(interface{}))
		}

		if err := rows.Scan(row...); err != nil {
			return fmt.Errorf("error scanning row: %v", err)
		}

		for i, c := range columns {
			d.Data[c] = append(d.Data[c], *row[i].(*interface{}))
		}
	}

	return nil
}

// ToSqlTable creates a new table in the database and inserts the dataframe data
func (d *DataFrame) ToSqlTable(ctx context.Context, db DBTX, table string) error {

	columns := ""
	for i, c := range d.Columns {
		if i > 0 {
			columns += ", "
		}
		columns += c
	}

	// use create or replace statement
	// to avoid error if table already exists
	query := fmt.Sprintf("CREATE OR REPLACE TABLE %v (", table)

	// add columns data types
	for _, c := range d.Columns {
		// detect data type
		var dataType string
		switch d.Data[c][0].(type) {
		case int:
			dataType = "INT"
		case float64:
			dataType = "FLOAT"
		default:
			dataType = "STRING"
		}

		query += fmt.Sprintf("%v %v, ", c, dataType)
	}

	// remove trailing comma and space
	query = query[:len(query)-2]

	// add closing parenthesis
	query += ");"

	_, err := db.ExecWithContext(ctx, query)
	if err != nil {
		return fmt.Errorf("error creating table: %v", err)
	}

	// insert data
	for i := 0; i < len(d.Data[d.Columns[0]]); i++ {
		query = fmt.Sprintf("INSERT INTO %v (%v) VALUES (", table, columns)
		for j, c := range d.Columns {
			if j > 0 {
				query += ", "
			}
			query += fmt.Sprintf("'%v'", d.Data[c][i])
		}
		query += ");"
	}

	_, err = db.ExecWithContext(ctx, query)
	if err != nil {
		return fmt.Errorf("error inserting data: %v", err)
	}

	return nil
}

// Concat
func (d *DataFrame) Concat(others ...*DataFrame) error {

	for _, o := range others {
		if len(d.Columns) != len(o.Columns) {
			return fmt.Errorf("dataframes have different number of columns")
		}

		for _, c := range d.Columns {
			if d.Data[c] == nil {
				d.Data[c] = make([]interface{}, 0)
			}
			d.Data[c] = append(d.Data[c], o.Data[c]...)
		}
	}

	return nil
}

// Head
func (d *DataFrame) Head(n int) *DataFrame {
	h := NewDataFrame()
	h.Columns = d.Columns
	for _, c := range d.Columns {
		h.Data[c] = d.Data[c][:n]
	}
	return h
}

// Shape
func (d *DataFrame) Shape() (int, int) {
	return len(d.Columns), len(d.Data[d.Columns[0]])
}

// Pivot
func (d *DataFrame) Pivot(index, columns, values string) *DataFrame {
	p := NewDataFrame()
	p.Columns = append(p.Columns, columns)
	for _, c := range d.Data[columns] {
		p.Data[columns] = append(p.Data[columns], c)
	}
	for _, i := range d.Data[index] {
		if p.Data[fmt.Sprintf("%v", i)] == nil {
			p.Columns = append(p.Columns, fmt.Sprintf("%v", i))
		}
		for _, c := range d.Data[columns] {
			if p.Data[fmt.Sprintf("%v", i)] == nil {
				p.Data[fmt.Sprintf("%v", i)] = make([]interface{}, len(d.Data[columns]))
			}
			for j, v := range d.Data[values] {
				if d.Data[index][j] == i && d.Data[columns][j] == c {
					p.Data[fmt.Sprintf("%v", i)][j] = v
				}
			}
		}
	}
	return p
}

// String
func (d *DataFrame) String() string {
	s := ""
	for _, c := range d.Columns {
		s += fmt.Sprintf("%v\t", c)
	}
	s += "\n"
	for i := 0; i < len(d.Data[d.Columns[0]]); i++ {
		for _, c := range d.Columns {
			s += fmt.Sprintf("%v\t", d.Data[c][i])
		}
		s += "\n"
	}
	return s
}
