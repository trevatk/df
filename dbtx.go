package df

import (
	"context"
	"database/sql"
)

// DBTX
type DBTX interface {
	ExecWithContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	QueryWithContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
}
