package df_test

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/trevatk/df"
)

func Test_SimpleDF(t *testing.T) {
	t.Run("simpleDF", func(t *testing.T) {
		df := df.NewDataFrame()
		df.AddColumn("name", []interface{}{"a", "b", "c"})
		df.AddColumn("age", []interface{}{1, 2, 3})
		fmt.Println(df.String())
	})
}

func Test_NewDataFrameFromCSV(t *testing.T) {
	t.Run("newDataFrameFromCSV", func(t *testing.T) {
		f, _ := os.Open("./testfiles/csv/x.csv")
		df, _ := df.NewDataFrameFromCSV(f)
		fmt.Println(df.String())
	})
}

func Test_DropDuplicates(t *testing.T) {
	t.Run("dropDuplicate", func(t *testing.T) {
		df := df.NewDataFrame()
		df.AddColumn("name", []interface{}{"a", "b", "c", "a"})
		df.AddColumn("age", []interface{}{1, 2, 3, 1})
		fmt.Println(df.String())
		df.DropDuplicates()
		fmt.Println(df.String())
	})
}

func Test_FillNA(t *testing.T) {
	t.Run("fillNA", func(t *testing.T) {
		df := df.NewDataFrame()
		df.AddColumn("name", []interface{}{"a", "b", nil, "a"})
		df.AddColumn("age", []interface{}{1, 2, 3, nil})
		fmt.Println(df.String())
		df.FillNA(0)
		fmt.Println(df.String())
	})
}
