package examples

import (
	"fmt"

	"gitlab.com/trevatk/df"
)

func Example_simpleDF() {
	df := df.NewDataFrame()
	df.AddColumn("name", []interface{}{"a", "b", "c"})
	df.AddColumn("age", []interface{}{1, 2, 3})
	// fmt.Println(df)
	// Output:
	// name age
	// a 1
	// b 2
	// c 3
	fmt.Println(df.String())
}
